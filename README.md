https://dishcandanty.gitlab.io/dragonruby-docs

# DragonRuby Docs

POC for restructure of Dragon Ruby Docs

## Adding Documentation

1. Create new file in `src/docs`
2. Do the documentation

### Single Page

```javascript
import flux from "@aust/react-flux";

const Page = () => {
  return <div>Do the Thing!</div>;
};

flux.dispatch("docs/add", {
  name: "About",
  page: Page,
  weight: 10,
  file: "about", // Used for Edit Page
});
```

### Nested

```
flux.dispatch("docs/add", {
  group: "Intro Videos",
  name: "New to Ruby and Programming",
  page: Page,
  weight: 1,
  file: "new-to-ruby-programming", // Used for Edit Page
});


```

3. Create Merge Request
4. Magic
