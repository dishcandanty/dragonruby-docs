import { Typography } from "@mui/material";
import React from "react";
import { useTheme } from "@mui/styles";

function Header({
  size = "h5",
  children = "Default header!",
  sx = {
    borderBottom: "",
    marginBottom: 1,
    paddingBottom: 0.4,
    color: "text.primary",
  },

  fontFamily = "Macondo Swash Caps",

  top = 5,
}) {
  const { palette } = useTheme();
  sx.borderBottom = `solid 1px ${palette.text.primary}`;
  if (top) sx.marginTop = top;
  if (fontFamily) sx.fontFamily = fontFamily;

  return (
    <Typography variant={size} sx={sx}>
      {children}
    </Typography>
  );
}

export default Header;
