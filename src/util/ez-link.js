import { Link } from "@mui/material";
import React from "react";

function EzLink({
  href = "http://docs.dragonruby.org/",
  target = "_blank",
  rel = "noreferrer",
  text = null,
  underline = "none",
  children = null,
}) {
  return (
    <Link href={href} target={target} rel={rel} underline={underline}>
      {text || children || href}
    </Link>
  );
}

export default EzLink;
