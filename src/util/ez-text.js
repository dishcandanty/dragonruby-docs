import { Typography } from "@mui/material";
import React from "react";

function EzText({
  children = "default text!",
  fontStyle = null,
  color = "text.primary",
  sx = {},
}) {
  sx.fontStyle = fontStyle;
  sx.color = color;
  return <Typography sx={sx}>{children}</Typography>;
}

export default EzText;
