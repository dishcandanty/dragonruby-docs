// New Image Helper that uses aspect ratio!
import React from "react";

function ImageUrl({
  max = null,
  min = null,
  circle = false,
  size = null,
  unit = "vh",
  aspectRatio = null,
  margin = null,
  // margin = 'auto',
  // aspectRatio = '1 / 1',
  url,
}) {
  let image = {
    flex: 1,
    aspectRatio: aspectRatio,
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center center",
    margin: margin,
    backgroundImage: `url(${url})`,
  };

  if (max) {
    image.maxHeight = `${max}${unit}`;
    image.maxWidth = `${max}${unit}`;
  }

  if (min) {
    image.minHeight = `${min}${unit}`;
    image.minWidth = `${min}${unit}`;
  }

  // Size Override for both
  if (size) {
    image.minHeight = `${size}${unit}`;
    image.minWidth = `${size}${unit}`;
  }

  if (circle) {
    image.borderRadius = "50%";
    image.overflow = "hidden";
  }

  return <div style={image}></div>;
}

export default ImageUrl;
