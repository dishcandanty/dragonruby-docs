import React from "react";
import Div from "./div";

export default function Center(props = {}) {
  return (
    <Div justify='center' align='center' direction='column' {...props}></Div>
  );
}
