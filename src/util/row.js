import React from "react";
import Div from "./div";

export default function Row(props = {}) {
  return <Div {...props} direction='row'></Div>;
}
