import React from "react";
import Div from "./div";

export default function Column(props = {}) {
  return <Div {...props} direction='column'></Div>;
}
