import React from "react";
import flux from "@aust/react-flux";
import { useTheme } from "@mui/material/styles";

// Local
import Page from "view/page";

const Nav = () => {
  const { status, context } = flux.sys.useState();
  const theme = useTheme();

  return (
    <div style={style(theme)}>
      {status !== "loading" && <Page status={status} context={context} />}
    </div>
  );
};

export default Nav;

const style = (theme) => {
  return {
    display: "flex",
    flex: 1,
    height: "100vh",
    width: "100vw",
    flexDirection: "column",
    backgroundColor: theme.palette.background.paper,
  };
};
