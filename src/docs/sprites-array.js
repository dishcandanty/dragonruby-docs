import flux from "@aust/react-flux";
import { Box } from "@mui/system";
import Code from "util/code";
import Header from "util/header";
import EzText from "util/ez-text";

const codeBlock = `
# X    Y    WIDTH   HEIGHT                        PATH
[100, 100,   160,     90,   "sprites/circle/white.png]
`;

const argsBlock = `
def tick args
  #                         X    Y   WIDTH   HEIGHT                      PATH
  args.outputs.sprites << [100, 100,   160,     90, "sprites/circle/white.png]
end
`;

const arrayBlockColor = `
#  X    Y  WIDTH  HEIGHT           PATH                ANGLE  ALPHA  RED  GREEN  BLUE
[100, 100,  160,     90, "sprites/circle/white.png",     0,    128,   0,   255,    0]
`;

const Page = () => {
  return (
    <Box>
      <Header size='h3' top={false}>
        Rendering a sprite using an Array
      </Header>

      <EzText>
        Creates a sprite of a white circle located at 100, 100. 160 pixels wide
        and 90 pixels tall.
      </EzText>

      <Code>{codeBlock}</Code>
      <Code>{argsBlock}</Code>

      <Header fontFamily='Macondo'>Array with colors and alpha</Header>
      <EzText>
        The value for the color and alpha is a number between 0 and 255. The
        alpha property is optional and will be set to 255 if not specified.
        Creates a green circle sprite with an opacity of 50%.
      </EzText>
      <Code>{arrayBlockColor}</Code>
    </Box>
  );
};

flux.dispatch("docs/add", {
  group: "Sprites",
  name: "via Array",
  page: Page,
  weight: 2,
  file: "sprites-array", // Used for Edit Page
});
