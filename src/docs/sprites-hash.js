import flux from "@aust/react-flux";
import { Box } from "@mui/system";
import Code from "util/code";
import Header from "util/header";
import EzText from "util/ez-text";

const hashCode = `
{
  x:                             0,
  y:                             0,
  w:                           100,
  h:                           100,
  path: "sprites/circle/white.png",
  angle:                         0,
  a:                           255,
  r:                             0,
  g:                           255,
  b:                             0
}`;

const argsCode = `
def tick(args)
  args.outputs.sprites << solid
end
`;

const Page = () => {
  return (
    <Box>
      <Header size='h3' top={false}>
        Rendering a sprite using a Hash
      </Header>

      <EzText>
        If you want a more readable invocation. You can use the following hash
        to create a sprite. Any parameters that are not specified will be given
        a default value. The keys of the hash can be provided in any order.
      </EzText>

      <Code>{hashCode}</Code>

      <Code>{argsCode}</Code>
    </Box>
  );
};

flux.dispatch("docs/add", {
  group: "Sprites",
  name: "via Hash",
  page: Page,
  weight: 4,
  file: "sprites-hash", // Used for Edit Page
});
