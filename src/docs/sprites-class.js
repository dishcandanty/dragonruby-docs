import flux from "@aust/react-flux";
import { Box } from "@mui/system";
import Code from "util/code";
import Header from "util/header";
import EzText from "util/ez-text";

const spriteClass = `
class Sprite
  attr_accessor :x, :y, :w, :h, :path, :angle, :angle_anchor_x, :angle_anchor_y,
                :tile_x, :tile_y, :tile_w, :tile_h, :source_x, :source_y,
                :source_w, :source_h, :flip_horizontally, :flip_vertically,
                :a, :r, :g, :b

  def primitive_marker
    :sprite
  end
end
`;

const circleClass = `
class Circle < Sprite
# constructor
  def initialize x, y, size, path
    self.x = x
    self.y = y
    self.w = size
    self.h = size
    self.path = path
  end
  def serlialize
    {x:self.x, y:self.y, w:self.w, h:self.h, path:self.path}
  end

  def inspect
    serlialize.to_s
  end

  def to_s
    serlialize.to_s
  end
end
`;

const tickArgs = `def tick args
# render circle sprite
args.outputs.sprites  << Circle.new(10, 10, 32,"sprites/circle/white.png")
end`;

const Page = () => {
  return (
    <Box>
      <Header size='h3' top={false}>
        Rendering a solid using a Class
      </Header>

      <EzText fontSize={"1.1rem"}>
        You can also create a class with solid/border properties and render it
        as a primitive. ALL properties must be on the class.{" "}
        <strong>Additionally</strong>, a method called{" "}
        <code>primitive_marker</code> must be defined on the class.
      </EzText>
      <EzText>Here is an example:</EzText>
      <br />

      <EzText>
        Create type with ALL sprite properties AND primitive_marker
      </EzText>
      <Code>{spriteClass}</Code>

      <EzText>Inherit from type</EzText>
      <Code>{circleClass}</Code>

      <EzText>Tick Render</EzText>
      <Code>{tickArgs}</Code>
    </Box>
  );
};

flux.dispatch("docs/add", {
  group: "Sprites",
  name: "via Class",
  page: Page,
  weight: 5,
  file: "sprites-class", // Used for Edit Page
});
