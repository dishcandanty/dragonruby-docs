// import flux from "@aust/react-flux";
// import { Divider, Typography } from "@mui/material";
// import { Box } from "@mui/system";
// import Code from "util/code";
// import Header from "util/header";

// const hashCode = `{
//   x:                             0,
//   y:                             0,
//   w:                           100,
//   h:                           100,
//   path: "sprites/circle/white.png",
//   angle:                         0,
//   a:                           255,
//   r:                             0,
//   g:                           255,
//   b:                             0
// }`;

// const argsCode = `
// def tick(args)
//   args.outputs.sprites << array
//   args.outputs.sprites << hash
// end
// `;

// const array = `
// # X    Y    WIDTH   HEIGHT                        PATH
// [100, 100,   160,     90,   "sprites/circle/white.png]
// `;

// const arrayColor = `
//   X    Y  WIDTH  HEIGHT           PATH                ANGLE  ALPHA  RED  GREEN  BLUE
// [100, 100,  160,     90, "sprites/circle/white.png",     0,    128,   0,   255,    0]
// `;

// const Page = () => {
//   return (
//     <Box>
//       <Header size='h3' top={false}>
//         Sprites
//       </Header>

//       <Typography>
//         Add primitives to this collection to render a sprite to the screen.
//       </Typography>
//       <Code>{argsCode}</Code>

//       <Header>Rendering a sprite using an Array</Header>
//       <Typography>
//         If you want a more readable invocation. You can use the following hash
//         to create a sprite. Any parameters that are not specified will be given
//         a default value. The keys of the hash can be provided in any order.
//       </Typography>
//       <Code>{array}</Code>

//       <Header>Rendering a sprite using an Array with colors and alpha</Header>
//       <Typography>
//         The value for the color and alpha is a number between 0 and 255. The
//         alpha property is optional and will be set to 255 if not specified.
//         Creates a green circle sprite with an opacity of 50%.
//       </Typography>
//       <Code>{arrayColor}</Code>

//       <Header>Rendering a sprite using a Hash</Header>
//       <Typography>
//         If you want a more readable invocation. You can use the following hash
//         to create a sprite. Any parameters that are not specified will be given
//         a default value. The keys of the hash can be provided in any order.
//       </Typography>
//       <Code>{hashCode}</Code>

//       <Header>Rendering a sprite using a Hash</Header>
//       <Typography>
//         If you want a more readable invocation. You can use the following hash
//         to create a sprite. Any parameters that are not specified will be given
//         a default value. The keys of the hash can be provided in any order.
//       </Typography>
//       <Code>{hashCode}</Code>

//       <Header>Rendering a solid using a Class</Header>
//       <Typography>
//         If you want a more readable invocation. You can use the following hash
//         to create a sprite. Any parameters that are not specified will be given
//         a default value. The keys of the hash can be provided in any order.
//       </Typography>
//       <Code>{hashCode}</Code>
//     </Box>
//   );
// };

// flux.dispatch("docs/add", {
//   group: "Sprites",
//   name: "Sprites",
//   page: Page,
//   weight: 5,
//   file: "sprites", // Used for Edit Page
// });
