import flux from "@aust/react-flux";
import { Box } from "@mui/system";
import EzLink from "util/ez-link";
import Header from "util/header";
import EzText from "util/ez-text";

import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";

const Page = () => {
  return (
    <Box>
      <Header size='h3' top={false}>
        Intro Videos
      </Header>
      <EzText>Here are some videos to help you get the lay of the land.</EzText>

      <Header fontFamily='Macondo'>Quick Api Tour</Header>
      <EzLink href='https://youtu.be/ixw7TJhU08E'>
        Beginner Introduction to DragonRuby Game Toolkit
      </EzLink>

      <Header fontFamily='Macondo'>
        If You Are Completely New to Ruby and Programming
      </Header>

      <List>
        <ListItem>
          <EzLink href='https://youtu.be/HG-XRZ5Ppgc'>
            1. Intermediate Introduction to Ruby Syntax
          </EzLink>
        </ListItem>

        <ListItem>
          <EzLink href='https://youtu.be/N72sEYFRqfo'>
            2. Intermediate Introduction to Arrays in Ruby
          </EzLink>
        </ListItem>

        <ListItem>
          <EzLink href=' http://dragonruby.school'>
            3. You may also want to try this free course provided at
            http://dragonruby.school
          </EzLink>
        </ListItem>
      </List>

      <Header fontFamily='Macondo'>If You Have Game Dev Experience</Header>

      <List>
        <ListItem>
          <EzLink href='https://youtu.be/xZMwRSbC4rY'>
            Building Tetris - Part 1
          </EzLink>
        </ListItem>

        <ListItem>
          <EzLink href='https://youtu.be/C3LLzDUDgz4'>
            Building Tetris - Part 2
          </EzLink>
        </ListItem>

        <ListItem>
          <EzLink href='https://youtu.be/pCI90ukKCME'>
            Low Res Game Jam Tutorial
          </EzLink>
        </ListItem>
      </List>
    </Box>
  );
};

flux.dispatch("docs/add", {
  name: "Intro Videos",
  page: Page,
  weight: 1,
  file: "intro-videos", // Used for Edit Page
});
