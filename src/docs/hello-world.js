import flux from "@aust/react-flux";
import { Box, Paper } from "@mui/material";
import EzLink from "util/ez-link";
import Header from "util/header";
import EzText from "util/ez-text";

const paperStyle = {
  padding: 1,
  paddingLeft: 5,
  margin: 1,
  bgcolor: "divider",
};
const Page = () => {
  return (
    <Box>
      <Header size='h3' top={false}>
        Hello World!
      </Header>

      <EzText>
        Welcome to DragonRuby Game Toolkit. Take the steps below to get started.
      </EzText>

      <Header fontFamily='Macondo'>
        Join the Discord and Subscribe to the News Letter
      </Header>

      <EzText>
        Our Discord channel is <EzLink href='http://discord.dragonruby.org' />
      </EzText>
      <br />

      <EzText>
        The News Letter will keep you in the loop with regards to current
        DragonRuby Events: <br />
        <EzLink href='http://dragonrubydispatch.com' />
      </EzText>
      <br />

      <EzText>
        Those who use DragonRuby are called Dragon Riders. This identity is
        incredibly important to us. When someone asks you:
      </EzText>

      <Paper sx={paperStyle} elevation={0}>
        <EzText fontStyle='italic'>What game engine do you use?</EzText>
      </Paper>

      <EzText>Reply with: </EzText>
      <Paper sx={paperStyle} elevation={0}>
        <EzText fontStyle='italic'>I am a Dragon Rider.</EzText>
      </Paper>
    </Box>
  );
};

flux.dispatch("docs/add", {
  name: "Hello World",
  page: Page,
  weight: 0,
  file: "hello-world", // Used for Edit Page
});
