import flux from "@aust/react-flux";
import EzText from "util/ez-text";

const Page = () => {
  return <EzText>This is the About Page!</EzText>;
};

flux.dispatch("docs/add", {
  name: "About",
  page: Page,
  weight: 100,
  file: "about", // Used for Edit Page
});
