import React from "react";
import flux from "@aust/react-flux";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Link from "@mui/material/Link";
import Switch from "@mui/material/Switch";
import LightModeIcon from "@mui/icons-material/LightMode";
import DarkModeIcon from "@mui/icons-material/DarkMode";
// Local
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Div from "util/div";
// import Search from "./search";

function ToolBar() {
  const { mode } = flux.sys.useState();
  const handleChange = () =>
    flux.dispatch("sys/mode", { mode: mode === "dark" ? "light" : "dark" });

  return (
    <AppBar position='static'>
      <Toolbar sx={{ justifyContent: "flex-end" }}>
        <Div flex={0} align='center'>
          {mode === "light" && <LightModeIcon fontSize='small' />}
          {mode === "dark" && <DarkModeIcon fontSize='small' />}
          <Switch checked={mode === "dark"} onChange={handleChange} />
        </Div>

        {/* <Search /> */}
        <Link
          {...style.iconBtn}
          href='https://gitlab.com/dishcandanty/dragonruby-docs'
        >
          <FontAwesomeIcon color='orange' icon={["fab", "gitlab"]} />
        </Link>

        <Link {...style.iconBtn} href='https://dragonruby.org/'>
          <FontAwesomeIcon color='red' icon='dragon' />
        </Link>
      </Toolbar>
    </AppBar>
  );
}

export default ToolBar;

const style = {
  iconBtn: {
    underline: "none",
    rel: "noopener",

    target: "_blank",
    sx: {
      border: "1px solid rgba(255,255,255,0.4)",
      borderRadius: "25%",
      margin: 0.4,
      padding: "0.6rem 0.5rem 0.5rem 0.5rem",
    },
  },
};
