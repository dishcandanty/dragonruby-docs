import React, { Fragment, useState } from "react";
import flux from "@aust/react-flux";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import Collapse from "@mui/material/Collapse";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import Entry from "./entry";

function Group({ name, pages }) {
  const status = flux.sys.useState("status");
  const isGroupPage = pages.find((x) => x.idx === status); // Open For Nav
  const [open, setOpen] = useState(isGroupPage !== undefined);
  const handleClick = () => setOpen(!open);

  return (
    <Fragment>
      <ListItemButton onClick={handleClick}>
        <ListItemText sx={{ color: "text.primary" }}>{name}</ListItemText>
        {open ? (
          <ExpandLess sx={{ color: "text.primary" }} />
        ) : (
          <ExpandMore sx={{ color: "text.primary" }} />
        )}
      </ListItemButton>

      <Collapse in={open} timeout='auto' unmountOnExit>
        <List component='div' disablePadding>
          {pages.map((x) => (
            <Entry key={x.name} entry={x} sx={{ pl: 4 }} />
          ))}
        </List>
      </Collapse>
    </Fragment>
  );
}

export default Group;
