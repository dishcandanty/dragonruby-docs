import React, { Fragment } from "react";
import Group from "./group";
import Entry from "./entry";

function LocalList({ link, filter }) {
  if (filter) {
    return <Entry entry={link} />;
  } else {
    return (
      <Fragment>
        {!link.group && <Entry entry={link} />}
        {link.group && <Group {...link} />}
      </Fragment>
    );
  }
}

export default LocalList;
