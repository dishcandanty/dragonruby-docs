import React from "react";
import flux from "@aust/react-flux";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";

function Entry({ entry, sx = {} }) {
  let status = flux.sys.useState("status");

  // Navigation Filter
  const handleClick = () => flux.dispatch("sys/status", entry.idx, entry.name);

  if (entry.idx === status) {
    sx.color = "red";
    sx.bgcolor = "divider";
  } else {
    sx.color = "text.primary";
    sx.bgcolor = null;
  }

  return (
    <ListItem sx={sx} key={entry.name} disablePadding>
      <ListItemButton onClick={() => handleClick(entry)}>
        <ListItemText>{entry.name}</ListItemText>
      </ListItemButton>
    </ListItem>
  );
}

export default Entry;
