import React, { useState } from "react";
import flux from "@aust/react-flux";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import { IconButton, TextField } from "@mui/material";
import ClearIcon from "@mui/icons-material/Clear";
import { useTheme } from "@mui/styles";
import { isEmpty } from "lodash";

// Local
import Column from "util/column";
import LinkList from "./link-list";
import Div from "util/div";

const drawerWidth = 240;
function SideBar() {
  const { palette } = useTheme();

  // Filters
  const [filter, setFilter] = useState("");
  const handleFilter = (event) => {
    setFilter(event.target.value);
  };
  const clearFilter = () => setFilter("");

  const list = flux.docs.selectState("list", filter);

  return (
    <Column
      style={{
        minWidth: drawerWidth,
        borderRight: `1px solid ${palette.divider}`,
      }}
      flex={0}
    >
      <Typography
        variant='h6'
        color='error'
        align='center'
        sx={{ paddingTop: 1, margin: "none", fontFamily: "Macondo Swash Caps" }}
      >
        DragonRuby
      </Typography>

      <Typography
        variant='caption'
        color='error'
        align='center'
        sx={{ paddingBottom: 1 }}
      >
        3.15
      </Typography>

      <Divider />
      <Box sx={{ padding: 1 }}>
        <Div justify='center' align='center'>
          <TextField
            fullWidth
            label='Filter'
            variant='standard'
            value={filter}
            onChange={handleFilter}
          />

          <IconButton size='small' onClick={clearFilter}>
            <ClearIcon
              fontSize='inherit'
              sx={{ color: "text.primary", opacity: 0.4 }}
            />
          </IconButton>
        </Div>
      </Box>

      <List>
        {list.map((link, i) => (
          <LinkList key={i} link={link} filter={filter} />
        ))}

        {isEmpty(list) && (
          <Typography align='center' color='error'>
            No Results
          </Typography>
        )}
      </List>

      <Divider />
    </Column>
  );
}

export default SideBar;
