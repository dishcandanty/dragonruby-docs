import flux from "@aust/react-flux";
import { toast } from "react-toastify";

const toastSettings = {
  position: "top-left",
  autoClose: 3500,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
};
const store = flux.addStore("alert");

// Notification Kinds
store.register("alert/info", async (_, message) => {
  console.log("Info", message);
  toast.info(message, toastSettings);
});

// Notification Kinds
store.register("alert/warn", async (_, message) => {
  console.log("Warn", message);
  toast.warn(message, toastSettings);
});
// Notification Kinds
store.register("alert/error", async (_, message) => {
  console.log("Error", message);
  toast.error(message, toastSettings);
});
