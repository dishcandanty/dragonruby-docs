import flux from "@aust/react-flux";
// import Api from "util/api";

function initialSettings() {
  return {
    name: null,
    admin: false,
    id: null,
  };
}

const store = flux.addStore("user", initialSettings());

// Username Password Login
store.register("user/login", async (_, _params) => {
  flux.dispatch("sys/start");
  // let result = await Api.process("post", "/auth/password", params);

  // Created Session

  // if (result.status) {
  // localStorage.setItem("goalguild/token", result.data);
  // await flux.dispatch("sys/status", "loading");
  // flux.dispatch("sys/start");
  // }
});

// Token Login
// store.register("user/token", async (dispatch) => {
//   let result = await Api.process("post", "/auth/token");

//   // Created Session
//   if (result.status) {
//     dispatch("sys/start");
//   } else {
//     // Redirect
//     dispatch("sys/status", "login");
//     localStorage.removeItem("goalguild/token");
//   }
// });

// Logout
store.register("user/logout", async (dispatch) => {
  // Purge Session
  localStorage.removeItem("goalguild/token");
  dispatch("sys/status", "login");
});

// ========================================================================
// -- Data Loaders
// ========================================================================
// store.register("user/meta", async (dispatch) => {
//   let result = await Api.process("get", "/user");
//   if (result.status) dispatch("user/update", result.data);
// });

// ========================================================================
// -- Store Updates
// ========================================================================
store.register("user/update", async (_, payload) => {
  return (state) => ({ ...state, ...payload });
});
// ========================================================================
